$( document ).ready(function( $ ) {
    $( '#slider-pro' ).sliderPro({
        width: 1200,
        height: 800,
        fade: true,
        arrows: true,
        buttons: false,
        fullScreen: true,
        shuffle: true,
        // smallSize: 500,
        // mediumSize: 1000,
        // largeSize: 3000,
        // thumbnailArrows: true,
        autoplay: false
    });

    // Preloader
    setTimeout(function() {
        var preloader = $('#preloader');
        if( !preloader.hasClass('done') )
        {
            preloader.addClass('done');
        }
    }, 1000);

});

// document.body.onload = function() {
//
// }
