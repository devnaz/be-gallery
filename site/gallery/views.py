from django.shortcuts import render, render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponse
from .models import *

# Create your views here.

def home(request):
    seasons = Season.objects.filter(published=True)
    context = {
        'seasons': seasons
    }
    return render_to_response('gallery/index.html', context)


def albums(request):
    seasons = Season.objects.filter(published=True)
    context = {
        'title': 'Альбоми',
        'seasons': seasons,
    }
    return render_to_response('gallery/albums.html', context)


def photos(request, id):
    albums = Album.objects.filter(published=True).exclude(id=id).order_by('?')[:4]
    album = get_object_or_404(Album, id=id)
    context = {
        'title': 'Фотографії',
        'album': album,
        'albums': albums,
    }
    return render_to_response('gallery/photos.html', context)
