from django.db import models
from versatileimagefield.fields import VersatileImageField, PPOIField

from os import path
from uuid import uuid4


def upload_photos(instance, filename):
    f, ext = path.splitext(filename)
    name = '%s%s' %(uuid4().hex, ext)
    return 'gallery/{i}'.format(i=name)


class Season(models.Model):
    title = models.CharField(max_length=30)
    rate = models.PositiveIntegerField(default=0)
    published = models.BooleanField(default=True)
    # date = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Сезон'
        verbose_name_plural = 'Сезони'
        ordering = ['-rate']


class Album(models.Model):
    title = models.CharField(max_length=50)
    date = models.DateField(auto_now=False, auto_now_add=True)
    season = models.ForeignKey('Season', related_name='albums', on_delete=models.CASCADE)
    published = models.BooleanField(default=True)

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = 'Альбом'
        verbose_name_plural = 'Альбоми'
        ordering = ['date']


class Photo(models.Model):
    image = VersatileImageField(upload_to=upload_photos, ppoi_field='ppoi', null=True, blank=True)
    ppoi = PPOIField(default='0.5x0.5')
    album = models.ForeignKey('Album', related_name='photos', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Фотографія'
        verbose_name_plural = 'Фотографії'
        # ordering = ['pk']

    def show_title(self):
        img = str(self.image).split('/')[-1]
        title = img.split('.')[0]
        return str(title)

    def show_previw_image(self):
        if self.image:
            return u'<img src="%s" width="100" />' %(self.image.url)
        return 'Image not found'
    show_previw_image.short_description = 'Превю'
    show_previw_image.allow_tags = True

    def save(self, *args, **kwargs):
        # do something
        qs = super(Photo, self).save(*args, **kwargs)
        if self.image == None or self.image == '':
            self.delete()
        return qs
