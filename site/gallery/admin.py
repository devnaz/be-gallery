from django.contrib import admin
from .models import *


class SeasonAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['title', 'rate', 'published']})
    ]


class PhotoInline(admin.StackedInline):
    model = Photo
    extra = 1


class AlbumAdmin(admin.ModelAdmin):
    inlines = [PhotoInline]


class PhotoAdmin(admin.ModelAdmin):
    add_form_template = 'new_change_form.html'
    list_display = ('show_title', 'show_previw_image')
    list_filter = ['album']

    def save_model(self, request, obj, form, change):
        images = request.FILES.getlist('images')
        album_id = request.POST.get('album')

        if images is not None or not []:
            for image in images:
                file_type = image.name.split('.')[-1]
                if file_type not in ['png', 'jpeg', 'jpg', ]:
                    raise Http404('Type of file is incorrect')
                obj.album_id = album_id
                obj.image = image
                obj.save()
        return super(PhotoAdmin, self).save_model(request, obj, form, change)


admin.site.register(Season, SeasonAdmin)
admin.site.register(Album, AlbumAdmin)
admin.site.register(Photo, PhotoAdmin)
