from django.conf.urls import url
from . import views as gallery_views

urlpatterns = [
    url(r'^$', gallery_views.home, name='home'),

    url(r'^albums/$', gallery_views.albums, name='albums'),
    # url(r'^album/$', gallery_views.albums, name='albums'),
    url(r'^album/(?P<id>\d+)/$', gallery_views.photos, name='photos'),
]
